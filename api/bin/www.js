'use strict'

var http = require('http')
  ,app = require('../app');

//CRIAÇÃO DO SERVER HTTP COM GERENCIADOR DE ROTAS DO APP
var httpServer = http.createServer(app);
// MANDAR SERVIDOR ESCUTAR PORTA 3000
     httpServer.listen(3000, function(){
       console.log('O servidor está no ar. Porta:' + 3000);
});
