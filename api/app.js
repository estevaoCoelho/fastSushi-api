'use strict';

var Food = require('./model/food-model')
  , db = require('./config/db-config')
  , express = require('express')
  , bodyParser = require('body-parser')
  , app = express();

  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());


//LIST ALL OF FOODS
app.get('/foodList', function(req, res){
  Food.findAll()
    .then(function(foods){
      console.log(foods);
      res.status(200).json(foods);
    })
    .catch(function(err){
      res.status(500).send(err);
    });
});
module.exports = app;
