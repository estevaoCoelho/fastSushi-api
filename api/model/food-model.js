'use strict';

var mongoose = require('mongoose')
  , foodSchema = new mongoose.Schema({
      name: { type: mongoose.Schema.Types.String, required: true, trim: true },
      price: { type: mongoose.Schema.Types.String, required: true },
      description: { type: mongoose.Schema.Types.String, required: true, trim: true },
      imageBytes: { type: mongoose.Schema.Types.String, required: true, trim: true },
    })
  , Food = mongoose.model('Food', foodSchema, 'Foods');

/**
 * Find all active Foods
 * @return {Promise}
 */
module.exports.findAll = function findAll(callback) {
  return new Promise(function(resolve, reject) {
Food.find(function(err, data){
  if (err) {
    reject(err);
  }
  else {
      resolve(data);
      console.log(data);
  }
    });
  });
};

/**
 * Find Food by _ID
 * @param  {String} foodId [required] ObjectId
 * @return {Promise}
 */
module.exports.findById = function findById(studentId) {
  return new Promise(function(resolve, reject){
    let query = {_id : studentId }
    Food.findOne(query, function(err, data){
      if (err) {
        reject(err);
      }else{
        resolve(data);
      }
    });
  });
};

/**
 * Insert Student
 * @param  {Student} student [required]
 * @return {Prmise}
 */
module.exports.save = function save(student) {
  return new Promise(function(resolve, reject){
    new Food(student).save(function(err, data){
      if (err) {
        reject(err);
      }else{
        resolve(data);
      }
    });
  });
};

/**
 * Modify Student
 * @param  {Student} student [required]
 * @return {Primise}
 */
module.exports.update = function update(student) {
  return new Promise(function(resolve, reject){
    let query = {_id : student._id};
    Food.update(query, student, function(err, data){
      if (err) {
        reject(err);
      }else {
        resolve(data);
      }
    });
  });

};

/**
 * Inactivate Student by _ID
 * @param  {String} studentId [required] ObjectId
 * @return {Promise}
 */
 module.exports.remove = function remove(studentId) {
   return new Promise(function(resolve, reject) {
     let query = { _id : studentId }
       , mod = { active: false };

     Food.update(query, mod, function(err, data) {
       if(err) {
         reject(err);
       } else {
         resolve(data);
       }
     });
   });
 };
